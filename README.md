# Azure DevOps Mão na Massa

### [Descubra o Azure DevOps: o Passo a Passo para usar o melhor em automação!](https://www.youtube.com/watch?v=acsTOfNJN3I&list=PLMFPOLE2cW1wJgmyhz4_Q3sGYOybRhrc1&index=1)

O Azure DevOps era uma plataforma da Microsoft que oferecia uma variedade de ferramentas para ajudar equipes de desenvolvimento de software a planejar, desenvolver, testar e entregar aplicativos com mais eficiência. Algumas das principais ferramentas incluídas no Azure DevOps eram as seguintes:

  * Azure Boards: Um sistema de gerenciamento de projetos que permitia a criação e o rastreamento de itens de trabalho, como tarefas, problemas, histórias de usuários e bugs.

  * Azure Repos: Um serviço de controle de versão baseado no Git, que permitia hospedar repositórios de código-fonte privados e colaborar em projetos de desenvolvimento de software.

  * Azure Pipelines: Uma ferramenta de integração contínua e entrega contínua (CI/CD) que automatizava a construção, teste e implantação de aplicativos em várias plataformas e ambientes.

  * Azure Test Plans: Uma solução para gerenciar testes manuais e exploratórios, permitindo que as equipes criassem, planejassem e executem cenários de teste.
  
  * Azure Artifacts: Um repositório de artefatos, como pacotes NuGet, npm, Maven, entre outros, que permite gerenciar e compartilhar bibliotecas e dependências de projetos.

  * Azure DevTest Labs: Uma ferramenta para gerenciar ambientes de teste e desenvolvimento na nuvem, permitindo que você crie e gerencie facilmente máquinas virtuais e ambientes de teste sob demanda.

Lembre-se de que o cenário de ferramentas e serviços da Microsoft pode evoluir ao longo do tempo, e é possível que novas funcionalidades tenham sido adicionadas ou que a nomenclatura tenha sido alterada. Para obter informações mais atualizadas sobre o Azure DevOps e suas ferramentas, recomendo consultar a documentação oficial da Microsoft ou a página do Azure DevOps.

### [Otimize suas Configurações Organizacionais no Azure DevOps](https://www.youtube.com/watch?v=rF2A2q8fpR8&list=PLMFPOLE2cW1wJgmyhz4_Q3sGYOybRhrc1&index=2)

No entanto, posso fornecer uma orientação geral sobre como organizar o ambiente no Azure DevOps para configurar variáveis, permissões de acesso, mecanismos de gerenciamento de projetos e processos de aplicativos com abordagens de DevOps.

  * Configuração de Variáveis:
    As variáveis no Azure DevOps são usadas para armazenar informações que podem ser usadas em tarefas de pipeline, como chaves de API, senhas, URLs, etc. Para configurar variáveis, siga os passos abaixo:

  * Acesse seu projeto no Azure DevOps.
    Navegue até a configuração do pipeline (Azure Pipelines).
    Selecione o pipeline em que deseja adicionar as variáveis.
    Clique em "Editar" para editar o pipeline.
    Procure por "Variáveis" ou "Variáveis de Ambiente" e adicione as variáveis necessárias.

  * Permissões de Acesso:
    No Azure DevOps, as permissões de acesso são gerenciadas em nível de projeto e podem ser configuradas para usuários e grupos. Para configurar permissões de acesso:

  * Acesse as configurações do projeto.
    Procure por "Permissões" ou "Controle de Acesso".
    Defina as permissões para os diferentes grupos de usuários ou usuários individuais.

  * Mecanismos de Gerenciamento de Projetos e Processos:
    O Azure DevOps oferece o Azure Boards para gerenciamento de projetos. É uma ferramenta de planejamento ágil que permite criar e rastrear itens de trabalho, estabelecer fluxos de trabalho personalizados e gerenciar o progresso do projeto. Para começar a usar o Azure Boards:

  * Acesse a guia "Boards" no Azure DevOps.
    Crie seus quadros de tarefas, histórias de usuários, bugs, etc.
    Personalize os estados e fluxos de trabalho conforme necessário para se adequarem ao seu processo de desenvolvimento.

  * Processo para Aplicações com Abordagens de DevOps:
    Implementar abordagens de DevOps envolve a automação e integração de várias etapas do desenvolvimento de software, como integração contínua, entrega contínua, testes automatizados e implantações frequentes. Para configurar um processo DevOps em seu ambiente:

  * Crie um pipeline no Azure Pipelines que englobe suas etapas de integração contínua e entrega contínua.
    Utilize ferramentas como o Azure Test Plans para gerenciar testes manuais e automatizados.
    Integre seus repositórios Git com os pipelines para acionar automaticamente a compilação e implantação sempre que houver alterações no código-fonte.

É importante lembrar que o processo de adoção de práticas de DevOps pode variar dependendo do tipo de aplicativo e das necessidades específicas do projeto. É recomendável pesquisar mais sobre as práticas de DevOps, consultar a documentação do Azure DevOps e experimentar em ambientes de teste antes de implementar mudanças em produção.

Caso precise de um guia mais detalhado e prático, recomendo procurar tutoriais e cursos especializados em DevOps com o Azure DevOps em plataformas de aprendizado online ou na documentação oficial da Microsoft.


### [Organize seu trabalho de forma não mecânica - Saiba tudo sobre o Azure Boards!](https://www.youtube.com/watch?v=rF2A2q8fpR8&list=PLMFPOLE2cW1wJgmyhz4_Q3sGYOybRhrc1&index=2)

O Azure Boards é uma poderosa ferramenta do Azure DevOps que permite o gerenciamento de projetos e rastreamento de itens de trabalho, como tarefas, histórias de usuários, bugs e muito mais. Ele oferece uma visibilidade completa do desenvolvimento e permite que as equipes colaborem e compartilhem status de forma eficiente. Aqui estão alguns passos para começar a usar o Azure Boards e melhorar suas tarefas:

  * Acesso ao Azure DevOps:
    Certifique-se de que você tem acesso ao Azure DevOps e ao projeto em que deseja trabalhar. Se não tiver uma conta, você pode criar uma gratuitamente na página do Azure DevOps.

  * Criar um novo projeto:
    Se você ainda não tiver um projeto, crie um novo projeto no Azure DevOps. Defina seu nome e configurações básicas conforme necessário.

  * Navegando no Azure Boards:
    Após criar ou acessar seu projeto, navegue até a guia "Boards" no menu principal do Azure DevOps. Aqui você encontrará todos os recursos relacionados ao gerenciamento de projetos e tarefas.

  * Criar itens de trabalho:
    Comece criando itens de trabalho para representar suas tarefas, histórias de usuários, bugs, etc. Clique em "Novo item" e escolha o tipo de item que deseja criar. Preencha os detalhes relevantes e salve o item.

  * Configurar colunas e fluxos de trabalho:
    Personalize o quadro (board) configurando as colunas para representar o fluxo de trabalho do seu projeto. Por exemplo, você pode ter colunas como "A fazer", "Em andamento" e "Concluído". Arraste e solte os itens de trabalho entre as colunas à medida que avançam no processo de desenvolvimento.

  * Utilizar modelos de trabalho prontos:
    O Azure Boards oferece diversos modelos de trabalho prontos, como Scrum, Agile e CMMI. Escolha o modelo que melhor se adapta ao seu projeto e equipe. Os modelos fornecem campos e processos predefinidos que podem ajudar a melhorar a organização e o acompanhamento das tarefas.

  * Biblioteca de controle de acesso:
    Gerencie as permissões e controle de acesso no projeto usando a biblioteca de controle de acesso do Azure DevOps. Defina quem pode ver, editar e gerenciar itens de trabalho, repositórios de código e pipelines.

  * Automatização com pipelines de trabalho:
    Integre seus pipelines de trabalho do Azure DevOps para automatizar a compilação, testes e implantação do código-fonte. Isso ajuda a alcançar a entrega contínua e melhora a eficiência do desenvolvimento.

  * Acompanhamento e relatórios:
    Aproveite os recursos de acompanhamento e geração de relatórios do Azure Boards para obter insights sobre o progresso do projeto, a produtividade da equipe e outras métricas importantes.

  * Colaboração e compartilhamento de status:
    Incentive a colaboração entre os membros da equipe usando os recursos de comentários e notificações do Azure Boards. Compartilhe regularmente o status do projeto para manter todos informados sobre o progresso e as próximas etapas.

Esses são apenas alguns dos recursos e práticas que você pode utilizar no Azure Boards para melhorar suas tarefas e fluxo de trabalho. O Azure DevOps oferece uma infinidade de recursos e integrações para ajudar equipes de desenvolvimento a adotarem abordagens de DevOps e melhorar a qualidade e a velocidade de seus projetos. Para aprender mais detalhes sobre o Azure Boards e como usá-lo de forma efetiva, você pode explorar a documentação oficial do Azure DevOps e buscar por tutoriais e cursos online específicos.

### [Azure Pipelines: Crie Builds Clássicos e YAML | Azure DevOps Mão na Massa](https://www.youtube.com/watch?v=NBguI2LK0xs&list=PLMFPOLE2cW1wJgmyhz4_Q3sGYOybRhrc1&index=4)

Criar pipelines no Azure DevOps. No entanto, posso fornecer uma explicação sobre como criar pipelines de Build usando os modelos Clássico e YAML no Azure DevOps, bem como como desabilitar os pipelines clássicos em um projeto.

1. Criando um Pipeline de Build no Modelo Clássico:

Passo 1: Acesse o Azure DevOps e navegue até o projeto em que deseja criar o pipeline de build.

Passo 2: Na guia "Pipelines", clique em "Novo pipeline" e selecione "Build".

Passo 3: Escolha o repositório do seu projeto e selecione o modelo "Visual Designer" para criar o pipeline no modelo clássico.

Passo 4: Personalize o pipeline de acordo com suas necessidades, adicionando tarefas de build, testes e implantação.

Passo 5: Salve o pipeline e execute-o para construir o seu aplicativo.

1. Criando um Pipeline de Build usando YAML:

Passo 1: Acesse o Azure DevOps e navegue até o projeto em que deseja criar o pipeline de build.

Passo 2: Na guia "Pipelines", clique em "Novo pipeline" e selecione "Azure Repos Git" ou a fonte do seu repositório.

Passo 3: Selecione o repositório do seu projeto e escolha "Configurar um pipeline" usando o arquivo YAML.

Passo 4: Crie o arquivo YAML para definir o seu pipeline de build, incluindo etapas de build, testes e implantação.

Passo 5: Salve o arquivo YAML e o Azure DevOps automaticamente detectará o pipeline configurado.

Passo 6: Execute o pipeline de build para construir o seu aplicativo.

1. Configurando Agentes:

O Azure DevOps utiliza agentes para executar as tarefas do pipeline de build. Você pode escolher entre agentes hospedados no Azure DevOps ou agentes auto-hospedados em suas máquinas locais ou em máquinas virtuais.

1. Desabilitando Pipelines Clássicos:

Se você quiser desabilitar pipelines clássicos em seu projeto e migrar completamente para pipelines YAML, siga estes passos:

Passo 1: Acesse o Azure DevOps e navegue até o projeto que você deseja configurar.

Passo 2: Vá para a guia "Pipelines" e clique em "Configurações" no canto inferior esquerdo.

Passo 3: Em "Configurações do projeto", selecione "Configurações do serviço" e, em seguida, clique em "Pipelines".

Passo 4: Em "Limites de build", desmarque a opção "Habilitar pipelines de versão antiga (Clássico)".

Passo 5: Salve as configurações para desabilitar os pipelines clássicos em seu projeto.

É importante lembrar que os pipelines clássicos ainda podem ser executados se já estiverem configurados, mas você não poderá criar novos pipelines clássicos depois de desabilitar essa opção.

Lembre-se de que a criação e configuração de pipelines no Azure DevOps podem variar dependendo das necessidades e do tipo de projeto. É sempre recomendável consultar a documentação oficial da Microsoft para obter informações atualizadas e detalhadas sobre como criar pipelines no Azure DevOps.